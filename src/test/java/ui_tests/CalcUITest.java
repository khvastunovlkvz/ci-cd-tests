package ui_tests;

import com.codeborne.selenide.Configuration;
import com.codeborne.selenide.Selenide;
import org.openqa.selenium.By;
import org.testng.Assert;
import org.testng.ITestContext;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
import services.RetryClass;

import java.util.Arrays;

import static com.codeborne.selenide.Selenide.*;

public class CalcUITest {

    @BeforeSuite
    public static void setUp(ITestContext context) {
        Arrays.stream(
                context
                .getAllTestMethods())
                .forEach(x->x.setRetryAnalyzerClass(RetryClass.class));
    }

    @BeforeTest
    public void openGoogle() {
        Configuration.headless = true;
        Selenide.open("http://google.com");
    }


    private void assertAnswer(String value){
        $(By.name("q")).setValue(value + "=").pressEnter();
        String answer = $("#cwos").getText();
        Assert.assertEquals("4", answer);
    }

    @Test
    public void testPlus(){
        assertAnswer("2+2");
    }
    @Test
    public void testPlus2(){
        assertAnswer("1+3");
    }
    @Test
    public void testMinus(){
        assertAnswer("8-4");
    }
    @Test
    public void testMultiply(){
        assertAnswer("2*2");
    }

}
