package services;

import org.testng.IRetryAnalyzer;
import org.testng.ITestResult;

public class RetryClass implements IRetryAnalyzer {

        private static int countInit = 0;
        private static final int countMax = 3;

        @Override
        public boolean retry(ITestResult iTestResult) {

                if(!iTestResult.isSuccess()){
                        if(countInit < countMax){
                                countInit++;
                                return true;
                        }
                }
                return false;
        }
}
