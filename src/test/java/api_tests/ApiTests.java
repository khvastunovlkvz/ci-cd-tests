package api_tests;

import io.restassured.RestAssured;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import org.asynchttpclient.util.Assertions;
import org.testng.Assert;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.Test;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;

import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.equalTo;

public class ApiTests {



        @BeforeSuite
        public static void setBaseUrl(){
            RestAssured.baseURI = "https://api.spacexdata.com/v4";
        }

        @Test
        public void checkCeoIsElonMusk(){
            given().get("/company")
                    .then().log().body()
                    .body("ceo", equalTo("Elon Musk"));
        }

        @Test
        public void checkLinksIsFour(){
            LinkedHashMap<String, String> links = given().get("/company")
                    .then().log().body()
                    .extract().body().jsonPath().get("links");
            Assert.assertEquals(4, links.size());
        }

        @Test
        public void crewMembersIs30(){
            ArrayList members = given().get("/crew")
                    .then().log().body()
                    .extract().body().as(ArrayList.class);
            Assert.assertTrue(members.size() == 30);
        }

        @Test
        public void firstMemberIsRobert(){
            ArrayList members = given().get("/crew")
                    .then()
                    .extract().body().as(ArrayList.class);

            LinkedHashMap firstMember = (LinkedHashMap) members.get(0);
            String id = (String) firstMember.get("id");
            String name1 = (String) firstMember.get("name");

            Response response = given().get("/crew/"+id)
                    .then().log().body()
                    .extract().response();

            JsonPath jsonPath = response.jsonPath();

            String name = jsonPath.getString("name");

            Assert.assertEquals("Robert Behnken", name);
        }

        @Test
        public void lastMemberHasNoLaunches(){
            List<MembersPojo> members = given().get("/crew")
                    .then()
                    .extract().body().jsonPath().getList("", MembersPojo.class);

            String lastMemberId = members.get(members.size()-1).getId() ;

            MembersPojo lastMember = given().get("/crew/"+lastMemberId)
                    .then().log().body()
                    .extract().as(MembersPojo.class);

            Assert.assertTrue(lastMember.getLaunches().size()==1);
        }





}
